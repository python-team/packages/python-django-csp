python-django-csp (3.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version (Closes: #1073413)
  * Standards-Version: 4.7.0 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Refresh patch: replace PEP8 & Flakes by Ruff
  * Remove obsolete patch, fixed upstream
  * Add build-dependency on pybuild-plugin-pyproject
  * Add build-dependency on python3-sphinx-rtd-theme

 -- Alexandre Detiste <tchet@debian.org>  Sun, 30 Jun 2024 13:19:31 +0200

python-django-csp (3.7-3) unstable; urgency=medium

  * Add patch for Django 4 compatibility (Closes: #1013555).
  * Bump Standards-Version to 4.6.1.0.
  * Depend on python3-all for autopkgtests.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Sat, 30 Jul 2022 21:49:13 +0000

python-django-csp (3.7-2) unstable; urgency=low

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-django-csp-doc: Add Multi-Arch: foreign.

  [ Diego M. Rodriguez ]
  * d/tests: specify import name

  [ Michael Fladischer ]
  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.1.
  * Use uscan version 4.
  * Enable upstream testsuite for autopkgtests.
  * Remove unnecessary autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Wed, 03 Nov 2021 16:18:18 +0000

python-django-csp (3.7-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.

 -- Michael Fladischer <fladi@debian.org>  Thu, 15 Oct 2020 11:45:12 +0200

python-django-csp (3.5-2) unstable; urgency=low

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.
  * Bump Standards-Version to 4.4.1.

  [ Michael Fladischer ]
  * Add patch to rename setup.cfg section to 'tool:pytest' (Closes: #950055).
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.
  * Clean up .pytest_cache/CACHEDIR.TAG to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Sat, 22 Feb 2020 21:50:03 +0100

python-django-csp (3.5-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Michael Fladischer ]
  * New upstream release.
  * Add debian/gbp.conf.
  * Clean up pytest artifacts to allow two builds in a row.
  * Bump Standards-Version to 4.3.0.

 -- Michael Fladischer <fladi@debian.org>  Fri, 01 Feb 2019 11:30:48 +0100

python-django-csp (3.4-1) unstable; urgency=low

  * Initial release (Closes: #906891).

 -- Michael Fladischer <fladi@debian.org>  Tue, 21 Aug 2018 19:24:37 +0200
